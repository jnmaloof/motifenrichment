# Motif Enrichment

This repo contains R code for looking for over-represented DNA motifs.  Typically one would be looking for over-representation of transcription factor binding sites in the promoters of a set of genes found be cluster or differential expression analysis.

This is set up for tomato genes but could easily be adapted to other organisms

## Requirements

You will need:

* A file with gene IDs for the "universe" of genes in your experiment.  Typically all genes detected as being expressed.
* A file with gene IDs for the "target" set where you want to test for enrichment
* A fasta file with promoter sequences (see instructions below).  If you are happy with 1000bp upstream of tomato ITAG2.4 then you can just download the file from the repo.
* A file with motifs.  One such file is provided.

## Promoters

If you want to get your own promoters, you can do so using Mikes [extract-utr.pl script](https://github.com/mfcovington/extract-utr)

The code below will generate 1000bp upstream of the ATG for tomato genes and is what was used to generate the file included in the repo.

    extract-utr.pl --gff_file=~/Sequences/ref_genomes/tomato/ITAG2.4_Chromo2.5/ITAG2.4_gene_models.gff3 \
    --genome_fa_file=~/Sequences/ref_genomes/tomato/ITAG2.4_Chromo2.5/S_lycopersicum_chromosomes.2.50.fa  \
    --cds_fa_file=~/Sequences/ref_genomes/tomato/ITAG2.4_Chromo2.5/ITAG2.4_cds.fasta  \
    --fiveprime --utr_length=1000 --gene_length=0 \
    --output_fa_file=ITAG2.4_1000bp_upstream.fa
    

Here is the call to generate the _S.pennellii_ promoters.

    extract-utr.pl --gff_file=spenn_v2.0_gene_models_annot.gff \
    --genome_fa_file=Spenn.fasta  \
    --cds_fa_file=Spenn-v2-cds-annot.fa  \
    --fiveprime --utr_length=1000 --gene_length=0 \
    --output_fa_file=Spenn_v2_1000bp_upstream.fa

## Demo files

The script is configured to use demo files to show functionality. 




